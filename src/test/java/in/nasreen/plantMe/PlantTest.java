package in.nasreen.plantMe;

import in.nasreen.plantMe.model.Plant;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class PlantTest {

    @Test
    public void PlantFieldsAreInitialized() {
        Plant dianthus = new Plant("Dianthus", Plant.Category.ORNAMENTALS, Plant.GrowthType.ANNUAL,
                false, true,null, "Pink", Plant.SunLight.FULL_SUN, 
                Plant.WaterConsumption.LIGHT);
        dianthus.setBloomColor("Pink");
        dianthus.setMaturedFruitColor("Blue");

        assertEquals(dianthus.getBloomColor(), Optional.of("Pink"));
        assertEquals(dianthus.getMaturedFruitColor(), null);

    }
}
