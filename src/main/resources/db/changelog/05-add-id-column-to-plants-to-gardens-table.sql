--liquibase formatted sql

--changeset nazreenpe:5

alter table plants_to_gardens
add id varchar (36) not null;

--rollback alter table plants_to_gardens drop column id;
