--liquibase formatted sql

--changeset nazreenpe:8

alter table plantings
add plant_count integer;

--rollback alter table plantings drop column plant_count;