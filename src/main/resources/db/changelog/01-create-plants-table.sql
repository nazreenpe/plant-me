--liquibase formatted sql

--changeset nazreenpe:1

create table plants (
 id varchar(36) not null,
 plant_name varchar not null,
 category varchar not null ,
 growth_type varchar not null,
 has_fruit boolean not null,
 has_bloom boolean not null,
 matured_fruit_color varchar ,
 bloom_color varchar,
 created_at timestamp with time zone ,
 updated_at timestamp with time zone
);

--rollback DROP TABLE plants