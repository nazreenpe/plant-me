--liquibase formatted sql

--changeset nazreenpe:4

create table plants_to_gardens (
plant_id varchar (36) not null ,
garden_id varchar (36) not null
);

--rollback DROP TABLE plants_to_gardens