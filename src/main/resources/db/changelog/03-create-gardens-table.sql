--liquibase formatted sql

--changeset nazreenpe:3

create table gardens (
id varchar (36) not null ,
garden_name varchar not null ,
created_at timestamp with time zone ,
updated_at timestamp with time zone
);

--rollback DROP TABLE gardens