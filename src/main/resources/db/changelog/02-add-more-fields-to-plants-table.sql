--liquibase formatted sql

--changeset nazreenpe:2

alter table plants
add sun_light varchar,
add water_consumption varchar;

--rollback alter table plants drop column sun_light, drop column water_consumption;
