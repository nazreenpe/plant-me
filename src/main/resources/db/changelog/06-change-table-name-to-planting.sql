--liquibase formatted sql

--changeset nazreenpe:6

alter table plants_to_gardens
rename to planting;

--rollback alter table planting rename to plants_to_gardens;
