--liquibase formatted sql

--changeset nazreenpe:7

alter table planting
rename to plantings;

alter table plantings
add created_at timestamp with time zone,
add updated_at timestamp with time zone;

--rollback alter table plantings rename to planting, drop column created_at, drop column updated_at ;
