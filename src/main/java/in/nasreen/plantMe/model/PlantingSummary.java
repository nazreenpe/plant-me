package in.nasreen.plantMe.model;

public class PlantingSummary {
    private String plantName;
    private String plantId;
    private int plantCount;

    public PlantingSummary() {
    }

    public PlantingSummary(Planting planting) {
        this.plantId = planting.getPlantId();
        this.plantCount = planting.getPlantCount();
    }

    public String getPlantName() {
        return plantName;
    }

    public String getPlantId() {
        return plantId;
    }

    public int getPlantCount() {
        return plantCount;
    }

    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    public void setPlantId(String plantId) {
        this.plantId = plantId;
    }

    public void setPlantCount(int plantCount) {
        this.plantCount = plantCount;
    }
}
