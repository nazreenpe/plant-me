package in.nasreen.plantMe.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "plants")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plant {
    @Id
    private String id;

    @Column(name = "plant_name")
    private String name;

    @Enumerated(EnumType.STRING)
    private Category category;

    @Enumerated(EnumType.STRING)
    @Column(name = "growth_type")
    private GrowthType growthType;

    @Column(name = "has_fruit")
    private Boolean hasFruit;

    @Column(name = "has_bloom")
    private Boolean hasBloom;

    @Column(name = "matured_fruit_color")
    private String maturedFruitColor;

    @Column(name = "bloom_color")
    private String bloomColor;

    @Enumerated(EnumType.STRING)
    @Column(name = "sun_light")
    private SunLight sunLight;

    @Enumerated(EnumType.STRING)
    @Column(name = "water_consumption")
    private WaterConsumption waterConsumption;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "plant")
    private Set<Planting> plantings;

    public enum Category {
        EDIBLES, ORNAMENTALS;
    }

    public enum GrowthType {
        ANNUAL, BIENNAL, PERENNIAL, SHRUB, TREE, BULB, CLIMBER,
        TRAILER, GROUND_COVER, GRASS, CONIFER, FERN, CACTUS,
        SUCCULUNT;
    }

    public enum SunLight {
        FULL_SUN, FULL_SUN_TO_PART_SHADE, PART_SHADE
    }

    public enum WaterConsumption {
        MODERATE, LIGHT
    }

    public Plant() {
    }

    public Plant(String name, Category category, GrowthType growthType, Boolean hasFruit, Boolean hasBloom,
                 String maturedFruitColor, String bloomColor, SunLight sunLight, WaterConsumption waterConsumption) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.category = category;
        this.growthType = growthType;
        this.hasFruit = hasFruit;

        if (hasFruit) {
            this.maturedFruitColor = maturedFruitColor;
        }

        this.hasBloom = hasBloom;

        if (hasBloom) {
            this.bloomColor = bloomColor;
        }

        this.sunLight = sunLight;
        this.waterConsumption = waterConsumption;
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Category getCategory() {
        return category;
    }

    public GrowthType getGrowthType() {
        return growthType;
    }

    public Boolean getHasFruit() {
        return hasFruit;
    }

    public Boolean getHasBloom() {
        return hasBloom;
    }

    public Optional<String> getMaturedFruitColor() {
        return Optional.ofNullable(maturedFruitColor);
    }

    public Optional<String> getBloomColor() {
        return Optional.ofNullable(bloomColor);
    }

    public SunLight getSunLight() {
        return sunLight;
    }

    public WaterConsumption getWaterConsumption() {
        return waterConsumption;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setGrowthType(GrowthType growthType) {
        this.growthType = growthType;
    }

    public void setHasFruit(Boolean hasFruit) {
        this.hasFruit = hasFruit;
    }

    public void setHasBloom(Boolean hasBloom) {
        this.hasBloom = hasBloom;
    }

    public void setMaturedFruitColor(String maturedFruitColor) {
        if (hasFruit) {
            this.maturedFruitColor = maturedFruitColor;
        }
    }

    public void setBloomColor(String bloomColor) {
        if (hasBloom) {
            this.bloomColor = bloomColor;
        }
    }

    public void setSunLight(SunLight sunLight) {
        this.sunLight = sunLight;
    }

    public void setWaterConsumption(WaterConsumption waterConsumption) {
        this.waterConsumption = waterConsumption;
    }
}
