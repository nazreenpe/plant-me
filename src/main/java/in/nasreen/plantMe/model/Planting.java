package in.nasreen.plantMe.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "plantings")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Planting {
    @Column(name = "plant_count")
    private int plantCount;

    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "plant_id")
    @JsonIgnore
    private Plant plant;

    @ManyToOne
    @JoinColumn(name = "garden_id")
    @JsonIgnore
    private Garden garden;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public Planting() {
        this.id = UUID.randomUUID().toString();
    }

    public Planting(Plant plant, int plantCount, Garden garden) {
        this();
        this.plant = plant;
        this.garden = garden;
        this.plantCount = plantCount;
    }

    public String getId() {
        return id;
    }

    public String getPlantId() {
        if (this.plant != null) {
            return plant.getId();
        } else {
            return null;
        }
    }

    public String getGardenId() {
        if (this.garden != null) {
            return garden.getId();
        } else {
            return null;
        }
    }

    public int getPlantCount() {
        return plantCount;
    }

    public void setPlantCount(int plantCount) {
        this.plantCount = plantCount;
    }
}
