package in.nasreen.plantMe.web;

import in.nasreen.plantMe.model.Plant;
import in.nasreen.plantMe.requests.PlantCreateRequest;
import in.nasreen.plantMe.requests.PlantUpdateRequest;
import in.nasreen.plantMe.service.PlantDataBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/plants")
public class PlantController {
    private PlantDataBaseService plantDataBaseService;

    @Autowired
    public PlantController(PlantDataBaseService plantDataBaseService) {
        this.plantDataBaseService = plantDataBaseService;
    }

    @RequestMapping(path = "",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Plant createPlant(@RequestBody PlantCreateRequest request) {
        return plantDataBaseService.add(request.getName(), request.getCategory(),
                request.getGrowthType(), request.getHasFruit(), request.getHasBloom(), request.getBloomColor(),
                request.getMaturedFruitColor(), request.getSunLight(), request.getWaterConsumption());
    }

    @RequestMapping(path = "",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plant> getAll() {
        return plantDataBaseService.getAll();
    }

    @RequestMapping(path = "",
    method = RequestMethod.DELETE)
    public void deleteAll() {
        plantDataBaseService.deleteAll();
    }

    @RequestMapping(path = "/{id}",
    method = RequestMethod.PATCH,
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Plant> update(@PathVariable(name = "id") String identifier,
    @RequestBody PlantUpdateRequest request) {
        return plantDataBaseService.update(identifier, request)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Plant> showById(@PathVariable(name = "id") String identifier) {
        return plantDataBaseService.findById(identifier)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteById(@PathVariable(name = "id") String identifier) {
        plantDataBaseService.delete(identifier);
    }

    @RequestMapping(path = "/search/{name}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Plant> searchByName(@PathVariable(name = "name") String name) {
        return plantDataBaseService.findByName(name)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/search",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plant> searchByCategory(@RequestParam("category") Optional<Plant.Category> category,
                                        @RequestParam("growth_type") Optional<Plant.GrowthType> growthType) {
        List<Plant> result = null;

        if (category.isPresent()) result = plantDataBaseService.searchByCategory(category.get());

        if (growthType.isPresent()) result = plantDataBaseService.searchByGrowthType(growthType.get());

        return result;
    }

}
