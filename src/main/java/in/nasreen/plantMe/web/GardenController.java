package in.nasreen.plantMe.web;

import in.nasreen.plantMe.model.Garden;
import in.nasreen.plantMe.model.PlantingSummary;
import in.nasreen.plantMe.requests.GardenCreateRequest;
import in.nasreen.plantMe.requests.GardenUpdateRequest;
import in.nasreen.plantMe.service.GardenDataBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/gardens")
public class GardenController {
    private GardenDataBaseService gardenDataBaseService;

    @Autowired
    public GardenController(GardenDataBaseService gardenDataBaseService) {
        this.gardenDataBaseService = gardenDataBaseService;
    }

    @RequestMapping(path = "",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Garden createGarden(@RequestBody GardenCreateRequest request) {
        return gardenDataBaseService.add(request.getName());
    }

    @RequestMapping(path = "",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Garden> getAll() {
        return gardenDataBaseService.getAll();
    }

    @RequestMapping(path = "",
            method = RequestMethod.DELETE)
    public void deleteAll() {
        gardenDataBaseService.deleteAll();
    }

    @RequestMapping(path = "/{id}",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Garden> update(@PathVariable(name = "id") String identifier,
                                         @RequestBody GardenUpdateRequest request) {
        return gardenDataBaseService.update(identifier, request)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Garden> showById(@PathVariable(name = "id") String identifier) {
        return gardenDataBaseService.findById(identifier)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteById(@PathVariable(name = "id") String identifier) {
        gardenDataBaseService.delete(identifier);
    }

    @RequestMapping(path = "/search/{name}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Garden> searchByName(@PathVariable(name = "name") String name) {
        return gardenDataBaseService.findByName(name)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}/plants",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PlantingSummary> getPlantsInAGarden(@PathVariable(name = "id") String id) {
        List<PlantingSummary> plants = gardenDataBaseService.getPlants(id);
        System.out.println(plants.toArray());
        return plants;
    }
}
