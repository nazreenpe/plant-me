package in.nasreen.plantMe.web;

import in.nasreen.plantMe.model.Planting;
import in.nasreen.plantMe.requests.PlantingCreateRequest;
import in.nasreen.plantMe.requests.PlantingUpdateRequest;
import in.nasreen.plantMe.service.PlantingDataBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/plantings")
public class PlantingController {
    private PlantingDataBaseService plantingDataBaseService;

    @Autowired
    public PlantingController(PlantingDataBaseService plantingDataBaseService) {
        this.plantingDataBaseService = plantingDataBaseService;
    }

    @RequestMapping(path = "",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Planting createPlanting(@RequestBody PlantingCreateRequest request) {
        return plantingDataBaseService.add(request.getPlantId(), request.getPlantCount(),
                request.getGardenId());
    }

    @RequestMapping(path = "",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Planting> getAll() {
        return plantingDataBaseService.getAll();
    }

    @RequestMapping(path = "",
            method = RequestMethod.DELETE)
    public void deleteAll() {
        plantingDataBaseService.deleteAll();
    }

    @RequestMapping(path = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Planting> showById(@PathVariable(name = "id") String identifier) {
        return plantingDataBaseService.findById(identifier)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteById(@PathVariable(name = "id") String identifier) {
        plantingDataBaseService.delete(identifier);
    }

    @RequestMapping(path = "/{id}",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Planting> updatePlantCount(
            @PathVariable(name = "id") String identifier,
            @RequestBody PlantingUpdateRequest request) {
        return plantingDataBaseService.update(identifier, request.getPlantCount())
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


}
