package in.nasreen.plantMe.requests;

import in.nasreen.plantMe.model.Plant;

public class PlantCreateRequest {
    private String name;
    private Plant.Category category;
    private Plant.GrowthType growthType;
    private Boolean hasFruit;
    private Boolean hasBloom;
    private String bloomColor;
    private String maturedFruitColor;
    private Plant.WaterConsumption waterConsumption;
    private Plant.SunLight sunLight;


    public PlantCreateRequest() {
    }


    public String getName() {
        return name;
    }

    public Plant.Category getCategory() {
        return category;
    }

    public Plant.GrowthType getGrowthType() {
        return growthType;
    }

    public Boolean getHasFruit() {
        return hasFruit;
    }

    public Boolean getHasBloom() {
        return hasBloom;
    }

    public Plant.WaterConsumption getWaterConsumption() {
        return waterConsumption;
    }

    public Plant.SunLight getSunLight() {
        return sunLight;
    }

    public String getBloomColor() {
        return bloomColor;
    }

    public String getMaturedFruitColor() {
        return maturedFruitColor;
    }
}
