package in.nasreen.plantMe.requests;

public class PlantingCreateRequest {
    private String plantId;
    private String gardenId;
    private int plantCount;

    public PlantingCreateRequest() {
    }

    public String getPlantId() {
        return plantId;
    }

    public String getGardenId() {
        return gardenId;
    }

    public int getPlantCount() {
        return plantCount;
    }
}
