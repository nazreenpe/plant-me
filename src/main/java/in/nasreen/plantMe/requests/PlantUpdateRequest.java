package in.nasreen.plantMe.requests;

import in.nasreen.plantMe.model.Plant;

public class PlantUpdateRequest {
    private String name;
    private Plant.Category category;
    private Plant.GrowthType growthType;
    private Boolean hasFruit;
    private Boolean hasBloom;
    private String maturedFruitColor;
    private String bloomColor;
    private Plant.SunLight sunLight;
    private Plant.WaterConsumption waterConsumption;

    public PlantUpdateRequest() {
    }

    public String getName() {
        return name;
    }

    public Plant.Category getCategory() {
        return category;
    }

    public Plant.GrowthType getGrowthType() {
        return growthType;
    }

    public Boolean getHasFruit() {
        return hasFruit;
    }

    public Boolean getHasBloom() {
        return hasBloom;
    }

    public String getMaturedFruitColor() {
        return maturedFruitColor;
    }

    public String getBloomColor() {
        return bloomColor;
    }

    public Plant.SunLight getSunLight() {
        return sunLight;
    }

    public Plant.WaterConsumption getWaterConsumption() {
        return waterConsumption;
    }
}
