package in.nasreen.plantMe.requests;

public class PlantingUpdateRequest {
    private int plantCount;

    public PlantingUpdateRequest() {
    }

    public int getPlantCount() {
        return plantCount;
    }
}
