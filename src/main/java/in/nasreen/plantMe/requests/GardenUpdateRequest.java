package in.nasreen.plantMe.requests;

public class GardenUpdateRequest {
    private String name;

    public GardenUpdateRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
