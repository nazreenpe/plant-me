package in.nasreen.plantMe.service;

import in.nasreen.plantMe.db.PlantRepository;
import in.nasreen.plantMe.model.Plant;
import in.nasreen.plantMe.requests.PlantUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlantDataBaseService {
    private PlantRepository plantRepository;

    @Autowired
    public PlantDataBaseService(PlantRepository PlantRepository) {
        this.plantRepository = PlantRepository;
    }

    public Plant add(String name, Plant.Category category,
                     Plant.GrowthType growthType, Boolean hasFruit, Boolean hasBloom, 
                     String bloomColor, String maturedFruitColor, Plant.SunLight sunLight, 
                     Plant.WaterConsumption waterConsumption) {
        Plant plant = new Plant(name, category, growthType, hasFruit, hasBloom, bloomColor,
                maturedFruitColor, sunLight, waterConsumption);
        return plantRepository.save(plant);
    }

    public Optional<Plant> findById(String identifier) {
        return plantRepository.findById(identifier);
    }

    public Optional<Plant> update(String identifier, PlantUpdateRequest request) {
        return plantRepository.findById(identifier)
                .map(plant -> {
                    if (request.getName() != null) {
                        plant.setName(request.getName());
                    }
                    if (request.getCategory() != null) {
                        plant.setCategory(request.getCategory());
                    }
                    if (request.getGrowthType() != null) {
                        plant.setGrowthType(request.getGrowthType());
                    }
                    if (request.getHasBloom() != null) {
                        plant.setHasBloom(request.getHasBloom());
                    }
                    if (request.getBloomColor() != null) {
                        plant.setBloomColor(request.getBloomColor());
                    }
                    if (request.getHasFruit() != null) {
                        plant.setHasFruit(request.getHasFruit());
                    }
                    if (request.getMaturedFruitColor() != null) {
                        plant.setMaturedFruitColor(request.getMaturedFruitColor());
                    }
                    if (request.getSunLight() != null) {
                        plant.setSunLight(request.getSunLight());
                    }
                    if (request.getWaterConsumption() != null) {
                        plant.setWaterConsumption(request.getWaterConsumption());
                    }
                    return plantRepository.save(plant);
                });

    }

    public List<Plant> getAll() {
        return (List<Plant>) plantRepository.findAll();
    }

    public void deleteAll() {
        plantRepository.deleteAll();
    }

    public Optional<Plant> findByName(String name) {
        return plantRepository.findByName(name);
    }

    public List<Plant> searchByCategory(Plant.Category category) {
            return plantRepository.findByCategory(category);
    }

    public List<Plant> searchByGrowthType(Plant.GrowthType growthType) {
        return plantRepository.findByGrowthType(growthType);
    }

    public void delete(String identifier) {
        plantRepository.deleteById(identifier);
    }
}



