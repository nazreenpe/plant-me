package in.nasreen.plantMe.service;

import in.nasreen.plantMe.db.GardenRepository;
import in.nasreen.plantMe.db.PlantRepository;
import in.nasreen.plantMe.db.PlantingRepository;
import in.nasreen.plantMe.model.Garden;
import in.nasreen.plantMe.model.Plant;
import in.nasreen.plantMe.model.Planting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlantingDataBaseService {
    private PlantingRepository plantingRepository;
    private PlantRepository plantRepository;
    private GardenRepository gardenRepository;

    @Autowired
    public PlantingDataBaseService(PlantingRepository plantingRepository,
                                   PlantRepository plantRepository, GardenRepository gardenRepository) {
        this.plantingRepository = plantingRepository;
        this.plantRepository = plantRepository;
        this.gardenRepository = gardenRepository;
    }

    public Planting add(String plantId, int plantCount, String gardenId) {
        Plant plant = plantRepository.findById(plantId).get();
        Garden garden = gardenRepository.findById(gardenId).get();
        Planting planting = new Planting(plant, plantCount, garden);
        return plantingRepository.save(planting);
    }

    public Optional<Planting>findById(String identifier) {
        return plantingRepository.findById(identifier);
    }


    public List<Planting>getAll() {
        return plantingRepository.findAll();
    }

    public void deleteAll() {
        plantingRepository.deleteAll();
    }

    public void delete(String identifier) {
        plantingRepository.deleteById(identifier);
    }

    public Optional<Planting> update(String identifier, int plantCount) {
        return plantingRepository.findById(identifier)
                .map(planting -> {
                    planting.setPlantCount(plantCount);
                    return plantingRepository.save(planting);
                });
    }
}



