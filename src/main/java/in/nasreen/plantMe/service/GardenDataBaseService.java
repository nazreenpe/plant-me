package in.nasreen.plantMe.service;

import in.nasreen.plantMe.db.GardenRepository;
import in.nasreen.plantMe.db.PlantRepository;
import in.nasreen.plantMe.db.PlantingRepository;
import in.nasreen.plantMe.model.Garden;
import in.nasreen.plantMe.model.PlantingSummary;
import in.nasreen.plantMe.requests.GardenUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GardenDataBaseService {
    private GardenRepository gardenRepository;
    private PlantingRepository plantingRepository;
    private PlantRepository plantRepository;

    @Autowired
    public GardenDataBaseService(GardenRepository gardenRepository,
                                 PlantingRepository plantingRepository,
                                 PlantRepository plantRepository) {
        this.gardenRepository = gardenRepository;
        this.plantingRepository = plantingRepository;
        this.plantRepository = plantRepository;
    }

    public Garden add(String gardenName) {
        Garden garden = new Garden(gardenName);
        return gardenRepository.save(garden);
    }

    public List<Garden> getAll() {
        return gardenRepository.findAll();
    }

    public void deleteAll() {
        gardenRepository.deleteAll();
    }

    public Optional<Garden> update(String identifier, GardenUpdateRequest request) {
        return gardenRepository.findById(identifier)
                .map(garden -> {
                    garden.setName(request.getName());
                    return gardenRepository.save(garden);
                });
    }

    public Optional<Garden> findById(String identifier) {
        return gardenRepository.findById(identifier);
    }

    public void delete(String identifier) {
        gardenRepository.deleteById(identifier);
    }

    public Optional<Garden> findByName(String name) {
        return gardenRepository.findByName(name);
    }

    public List<PlantingSummary> getPlants(String id) {
        return gardenRepository.findById(id)
                .map(garden -> plantingRepository.findByGarden(garden).stream()
                        .map(planting -> {
                            PlantingSummary plantingSummary = new PlantingSummary();
                            plantingSummary.setPlantId(planting.getPlantId());
                            plantingSummary.setPlantCount(planting.getPlantCount());
                            plantingSummary.setPlantName(
                                    plantRepository.findById(planting.getPlantId()).get().getName()
                            );
                            return plantingSummary;
                        })
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }
}
