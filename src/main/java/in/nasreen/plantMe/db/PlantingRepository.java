package in.nasreen.plantMe.db;

import in.nasreen.plantMe.model.Garden;
import in.nasreen.plantMe.model.Planting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantingRepository extends CrudRepository<Planting, String> {
    List<Planting> findAll();
    List<Planting> findByGarden(Garden id);
}
