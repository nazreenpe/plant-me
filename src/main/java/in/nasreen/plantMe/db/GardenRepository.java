package in.nasreen.plantMe.db;

import in.nasreen.plantMe.model.Garden;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface GardenRepository extends CrudRepository<Garden, String> {
    List<Garden> findAll();

    Optional<Garden> findByName(String name);
}
