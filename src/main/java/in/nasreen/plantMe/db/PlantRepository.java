package in.nasreen.plantMe.db;

import in.nasreen.plantMe.model.Plant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlantRepository extends CrudRepository<Plant, String> {

    Optional<Plant> findByName(String name);

    List<Plant> findByCategory(Plant.Category thisCategory);

    List<Plant> findByGrowthType(Plant.GrowthType growthType);
}
